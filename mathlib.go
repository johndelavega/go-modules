package mathlib

import "fmt"

const mVersion = "v0.1.0"

// Add two integer numbers
func Add(a int, b int) int {

	return a + b
}

// ModInfo returns info string
func ModInfo() string {

	return fmt.Sprintf("mathlib Version %s | gitlab.com/johndelavega/go-modules", mVersion)

}

// Version returns version string for debugging
func Version() string {

	return fmt.Sprintf("%s", mVersion)

}
